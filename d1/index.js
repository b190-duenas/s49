/*
	placeholder API: https://jsonplaceholder.typicode.com/posts
*/
// Get data
fetch('https://jsonplaceholder.typicode.com/posts').then((response)=>response.json()).then((data)=>showPosts(data));

/*
	showPosts
		it will recieve the posts as its parameters
			create a postEntries variable with empty string as its value

			in the posts parameter, perform forEach method and render the following html elements as the new value for the postEntries variable
				- create a div with id of "post-post.id"
					-create h3 element with the id of "post-title-post.id" with the value of post.title
					-create p element with the id of "post-body-post.id" with the value of post.body 
					- create 2 buttons
						- edit button with onclick event handler calling editPost function recieving post.id as its argument
						- delete button with onclick event handler calling deletePost function recieving post.id as its argument

			5 minutes - 6:02 pm (feel free to send the output in the google chat :) )
*/


// add post data
document.querySelector('#form-add-post').addEventListener('submit', (e)=>{
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts",{
		method:"POST",
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: { 'Content-type': 'application/json; charset=UTF-8' }
	})
	.then((response)=>response.json())
	.then((data)=>{
		console.log(data);
		alert('Successfully added.');

		// lets input fields to be blank after the alert is closed
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
});
// Show posts
const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries +=`
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p  id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost(${post.id})">Edit</button>
			<button onclick="deletePost(${post.id})">Delete</button>
		</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
};
// Edit Post
/*
	make the edit button work by creating editPost function
		the values recieved from the div, should be copied inside the input fields of the edit post form
			id
			title
			body

	5 mintues - 6:34 (feel free to send your output in the google chat)
*/

const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

// Update Post
/*
	for function, select the edit post form through the use of document.querySelector, it should listen to submit event and will perform the following block of codes
		- set the method to PUT
		- send the body as stringified json with the following properties:
			- id = id input field of the edit post form
			- title = title input field of the edit post form
			-body = body input field of the edit post form
			- userId = 1
		- set the headers for content type of application/json, charset=UTF-8
		- use then methods to convert the response into json and log it in the console
		- send an alert for successful updating
		- empty the edit post form after the codes has been executed

		6:55 pm - solution discussion (feel free to send your output in the chat)
*/	

document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: { 'Content-type': 'application/json; charset=UTF-8' }
	})
	.then((response)=>response.json())
	.then((data)=>{
		console.log(data);
		alert('Successfully added.');

		// lets the updated object to be displayed in the frontend c/o (Sir JC)
		document.querySelector(`#post-title-${data.id}`).innerHTML = data.title;
		document.querySelector(`#post-body-${data.id}`).innerHTML = data.body;

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		// setAttribute is used to allow setting up or bring back the removed attribute of the HTML elements
		/*
			it is accepting 2 arguments
				1st - the string that identifies the attribute to be set
				2nd - boolean to state whether the attribute is to be set (true); (false) is not accepted as the removing of the attribute; but removing the "true" would not work for the setAttribute 
		*/
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})
})

const deletePost = (id) =>{
	document.querySelector(`#post-${id}`).innerHTML = null;
}